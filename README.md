# TP 1

## Objectifs



Faire répondre une application web PHP basée sur une infrastructure HA Azure
(pas de base de données)



## Attentes



- Un groupe de ressources dédiés à ce travail
- Un répartiteur de charge en frontal avec une IP statique.
- Une sonde permettant de monitorer la santé de votre appli web (port 80)
- Un groupe de machines virtuelles (ScaleSet) disposant d'un disque supplémentaire de 20Go
- Les machines sont provisionnées grâce à un fichier de configuration de cloud-init.
- Mettre en place les groupes de sécurités permettant de sécuriser le réseau

## Les plus



- Ajouter une base de données managée et adapter l'application pour qu'elle l'utilise.

# Commandes 

## Création groupe de ressources

```bash
az group create --resource-group sciencesuPhPHADylan --location francecentral
```

## Création IP Public

```bash
az network public-ip create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanPublicIP --allocation-method static
```

```mermaid
flowchart LR
Navigateur <--> IPPublic["IP Publique"]
```

## Création du ScaleSet

```bash
az vmss create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanScaleSet --image UbuntuLTS --custom-data cloud_init.yml --upgrade-policy-mode automatic --admin-user sciencesu --ssh-key-values ./.ssh/id_rsa.pub --data-disk-sizes-gb 20 --public-ip-address sciencesuPhPHADylanPublicIP --no-wait
```

```mermaid
flowchart LR
Client <--> IPPublic["IP Publique"]
subgraph scaleset
vm1
vm2
LB["Load Balancer"]
reseaux["Réseau Privé"]
end
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux
reseaux --- vm1
reseaux --- vm2
```

## Création d'une sonde pour le Load Balancer

```bash
az network lb probe create --resource-group sciencesuPhPHADylan --lb-name sciencesuPhPHADylanScaleSetLB --name HTTPHealthProbe --protocol tcp --port 80
```

```mermaid
flowchart LR
Client <--> IPPublic["IP Publique"]
subgraph scaleset
vm1
vm2
LB["Load Balancer"]
reseaux["Réseau Privé"]
end
IPPublic <-->|"Frontend"| LB["Load Balancer"]
LB <-->|"Backend"| reseaux
reseaux --- vm1
reseaux --- vm2
sonde["Sonde port 80"] --- LB
```

## Création d'une règle d'équilibrage de charge pour le Load Balancer

```bash
az network lb rule create --resource-group sciencesuPhPHADylan --lb-name sciencesuPhPHADylanScaleSetLB --name HTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name loadBalancerFrontEnd --backend-pool-name sciencesuPhPHADylanScaleSetLBBEPool --probe-name HTTPHealthProbe
```

## Création du groupe de sécurité

```bash
az network nsg create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanNSG
```

# Ajout du Groupe de sécurité au sous-réseau

```bash
az network vnet subnet update --resource-group sciencesuPhPHADylan --vnet-name sciencesuPhPHADylanScaleSetVNET --name sciencesuPhPHADylanScaleSetSubnet --network-security-group sciencesuPhPHADylanNSG
```

## Création règle de sécurité

```bash
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSG --name HTTPNSGRule --priority 100 --protocol tcp --destination-port-range 80
```

```bash
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSG --name DenyAll --access Deny --protocol tcp --direction Inbound --priority 300  --source-address-prefix "*" --source-port-range "*" --destination-address-prefix "*" --destination-port-range "*"
```

```mermaid
flowchart LR
Client <--> IPPublic["IP Publique"]
subgraph scaleset
vm1
vm2
LB["Load Balancer"]
reseaux["Réseau Privé Sécurisé"]
end
IPPublic <-->|"Frontend"| LB
reseaux --- vm1
reseaux --- vm2
sonde["Sonde port 80"] --- LB
LB --x reseaux
LB ---|Port 80| reseaux
linkStyle 5 stroke:#ff0000,color:red;
linkStyle 6 stroke:#00ff00,color:green;
```

## Création du serveur SQL

```bash
az mysql flexible-server create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylansql --admin-user sciencesu --admin-password ESGI12345678! --location francecentral --sku-name Standard_B1s --vnet sciencesuPhPHADylanScaleSetVNET --address-prefixes 10.0.0.0/16 --subnet mySubnet --subnet-prefixes 10.0.10.0/24
```

```mermaid
flowchart LR
Client <--> IPPublic["IP Publique"]
subgraph scaleset
vm1
vm2
LB["Load Balancer"]
reseaux["Réseau Privé Sécurisé"]
end
IPPublic <-->|"Frontend"| LB
reseaux --- vm1
reseaux --- vm2
sonde["Sonde port 80"] --- LB
LB --x reseaux
LB ---|Port 80| reseaux
linkStyle 5 stroke:#ff0000,color:red;
linkStyle 6 stroke:#00ff00,color:green;
reseaux --- sql["Serveur SQL Flexible"]
```

## Création de la base de données

```bash
az mysql flexible-server db create --resource-group sciencesuPhPHADylan --server-name sciencesuPhPHADylansql --database-name tp1ha
```

## Création du groupe de sécurité

```bash
az network nsg create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanNSGSQL
```
# Ajout du Groupe de sécurité au sous-réseau

```bash
az network vnet subnet update --resource-group sciencesuPhPHADylan --vnet-name sciencesuPhPHADylanScaleSetVNET --name mySubnet --network-security-group sciencesuPhPHADylanNSGSQL
```

## Création règle de sécurité

```bash
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSGSQL --name HTTPNSGRule --priority 100 --protocol tcp --destination-port-range 3306
```

```bash
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSGSQL --name DenyAll --access Deny --protocol tcp --direction Inbound --priority 300  --source-address-prefix "*" --source-port-range "*" --destination-address-prefix "*" --destination-port-range "*"
```

```mermaid
flowchart LR
Client <--> IPPublic["IP Publique"]
subgraph scaleset
vm1
vm2
LB["Load Balancer"]
reseaux["Réseau Privé Sécurisé"]
end
IPPublic <-->|"Frontend"| LB
reseaux --- vm1
reseaux --- vm2
sonde["Sonde port 80"] --- LB
LB --x reseaux
LB ---|Port 80| reseaux
linkStyle 5 stroke:#ff0000,color:red;
linkStyle 6 stroke:#00ff00,color:green;
reseaux --x sql["Serveur SQL Flexible"]
reseaux ---|Port 3306| sql["Serveur SQL Flexible"]
linkStyle 7 stroke:#ff0000,color:red;
linkStyle 8 stroke:#00ff00,color:green;
```

## Tester la configuration 

```bash
curl <ip-public>
```

## Supprimer groupe de ressources 

/!\ Cela supprime l'intégralité des ressources dans le groupe de ressources.

```powershell
az group delete --resource-group sciencesuPhPHADylan --no-wait
```
