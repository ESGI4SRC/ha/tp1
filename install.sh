# Création groupe de ressources
az group create --resource-group sciencesuPhPHADylan --location francecentral
# Création IP Public
az network public-ip create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanPublicIP --allocation-method static
# Création du ScaleSet
az vmss create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanScaleSet --image UbuntuLTS --custom-data cloud_init.yml --upgrade-policy-mode automatic --admin-user sciencesu --generate-ssh-keys --data-disk-sizes-gb 20 --public-ip-address sciencesuPhPHADylanPublicIP
# Création d'une sonde pour le Load Balancer
az network lb probe create --resource-group sciencesuPhPHADylan --lb-name sciencesuPhPHADylanScaleSetLB --name HTTPHealthProbe --protocol tcp --port 80
# Création d'une règle d'équilibrage de charge pour le Load Balancer
az network lb rule create --resource-group sciencesuPhPHADylan --lb-name sciencesuPhPHADylanScaleSetLB --name HTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name loadBalancerFrontEnd --backend-pool-name sciencesuPhPHADylanScaleSetLBBEPool --probe-name HTTPHealthProbe
# Création du groupe de sécurité
az network nsg create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanNSG
# Ajout du Groupe de sécurité au sous-réseau
az network vnet subnet update --resource-group sciencesuPhPHADylan --vnet-name sciencesuPhPHADylanScaleSetVNET --name sciencesuPhPHADylanScaleSetSubnet --network-security-group sciencesuPhPHADylanNSG
# Création règle de sécurité
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSG --name HTTPNSGRule --priority 100 --protocol tcp --destination-port-range 80
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSG --name DenyAll --access Deny --protocol tcp --direction Inbound --priority 300  --source-address-prefix "*" --source-port-range "*" --destination-address-prefix "*" --destination-port-range "*"
# Création du serveur SQL
az mysql flexible-server create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylansql --admin-user sciencesu --admin-password ESGI12345678! --location francecentral --sku-name Standard_B1s --vnet sciencesuPhPHADylanScaleSetVNET --address-prefixes 10.0.0.0/16 --subnet mySubnet --subnet-prefixes 10.0.10.0/24
# Création de la base de données
az mysql flexible-server db create --resource-group sciencesuPhPHADylan --server-name sciencesuPhPHADylansql --database-name tp1ha
# Création du groupe de sécurité
az network nsg create --resource-group sciencesuPhPHADylan --name sciencesuPhPHADylanNSGSQL
# Ajout du Groupe de sécurité au sous-réseau
az network vnet subnet update --resource-group sciencesuPhPHADylan --vnet-name sciencesuPhPHADylanScaleSetVNET --name mySubnet --network-security-group sciencesuPhPHADylanNSGSQL
# Création règle de sécurité
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSGSQL --name HTTPNSGRule --priority 100 --protocol tcp --destination-port-range 3306
az network nsg rule create --resource-group sciencesuPhPHADylan --nsg-name sciencesuPhPHADylanNSGSQL --name DenyAll --access Deny --protocol tcp --direction Inbound --priority 300  --source-address-prefix "*" --source-port-range "*" --destination-address-prefix "*" --destination-port-range "*"
